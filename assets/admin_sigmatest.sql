CREATE TABLE contacts(
    `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
    `name` varchar(50) NOT NULL DEFAULT '',
    `email` varchar(30) NOT NULL DEFAULT '',
    `state` varchar(30) NOT NULL DEFAULT '',
    `city` varchar(50) NOT NULL DEFAULT '',
    PRIMARY KEY (`id`)
)