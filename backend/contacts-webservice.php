<?php

/* 
 * Micro service to save contacts
 */

include 'connection.php';

// Receive form data
$json = file_get_contents('php://input');

// Convert to PHP object
$data = json_decode($json, FALSE);

if(!empty($data)){
    switch($data->selectType){
        case 'save':
            $name = utf8_decode($data->name);
            $email = utf8_decode($data->email);
            $state = utf8_decode($data->department);
            $city = utf8_decode($data->city);
            
            if($data->id == ''){
                $query = "INSERT INTO contacts(name, email, state, city)
                          VALUES('$name', '$email', '$state', '$city') 
                         ";
            }
            
            if(mysqli_query($connect, $query)){
                $arrayRecords = [
                    "message" => "Tu información ha sido recibida satisfactoriamente",
                    "error" => 0
                ];
            }else{
                $arrayRecords = [
                    "message" => "Un problema ocurrió. No se pudo enviar la información",
                    "error" => 101
                ];
            }
            
            break;

    }
}

// Closed connection
mysqli_close($connect);

// Data encoding in json format
echo json_encode($arrayRecords, JSON_UNESCAPED_UNICODE);
