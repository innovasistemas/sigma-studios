# Sigma Studios

## Web system Sigma Studios

### Used tecnologies

**Operation System**
- Linux: Kernel 4.8.0-53-generic
- Distrib: Linux Mint 18.2 Sonya

**Browsers**
- Chrome 83.0.4103.116 (Build oficial) (64 bits). Plugin Moesif CORS 
- Firefox 78.0.1 (64-bit)
- Opera 63.0.3368.66

**Web Server**
- Apache 2.4.33 (Ubuntu)

**Database Engine**
- MySQL 14.14 Distrib 5.7.25, for Linux (x86_64)
- DBMS: MySQL Workbench 6.3.10 (64 bits) Community

**Code Editors**
- NetBeans IDE 8.2
- Sublime Text 3

**Documentation**
- Markdown

**Layout**
- HTML5
- CSS3
- BootStrap library 4.5.2 minified
- MDB (Material Design for BootStrap) library minified

**Scripts Frontend**
- Javascript engine V8
- jQuery library 3.4.1 minified
- jQuery library UI 1.12.1 minified
- jQuery plugin Select2 full minified

**Scripts Backend**
- PHP 7.0.33-55 (Ubuntu)
- Zend Engine v3.0.0



