/* 
 * Common scripts from website
 */


$(function(){
    // Cancel form submit event
    $('#frmRegister').on('submit', function(event){
        event.preventDefault();  
    });
    
    
    // Activate select2 plugin
    $('.select2').select2({
        tags: false,
        placeholder: 'Seleccione una opción',
        allowClear: true,
        width: '100%',
        language: {
            noResults: function(){
                return "No hay resultados";        
            },
            searching: function(){
                return "Buscando...";
            }
        },
        close: function(){
            
        }
       
    });
    
        
    // Event 'change' for department list 
    $('#cboDepartment').change(function(){
        var department = $(this).val();
        $.ajax({
            url: 'https://sigma-studios.s3-us-west-2.amazonaws.com/test/colombia.json',
//            url: 'http://localhost/sigma-studios/public/assets/api/colombia.json',
            type: 'GET',
            dataType: 'json',
            success: function(data) {
                var option = "<option></option>";
                if(department !== ""){
                    $.each(data, function(index, value){
                        if(department === index){
                            $.each(value, function(index2, value2){
                                option += "<option value='" + value2 + "'>" + value2 + "</option>";
                            });
                            $('#cboCity').html(option);
                            return;
                        }
                    });
                }else{
                   $('#cboCity').html(option);
                }
            }
        }).done(function(){

        });
    });
    
    
    // Event click for the submit button (save data)
    $('#btnSubmit').click(function(){
        if(validateData()){
            var objJson = {
                'id': '',
                'name': $("#txtName").val(),
                'email': $("#txtEmail").val(),
                'department': $("#cboDepartment").val(),
                'city': $("#cboCity").val(),
                'selectType': 'save'
            };
                        
            $.ajax({
                url: '../backend/contacts-webservice.php',
                type: 'POST',
                dataType: 'json',
                data: JSON.stringify(objJson),
                success: function(data){
                    showMessage(data.message);
                    resetForm();
                }
            }).done(function(){

            });
        }
    });
    
    
    // Data validation at the focusout event of the form elements
    
    $('#cboDepartment').on('select2:close', function(){
        validateLength($('#lblDepartmentError'), $('#cboDepartment'), 3, 30);
    });
    
    
    $('#cboCity').on('select2:close', function(){
        validateLength($('#lblCityError'), $('#cboCity'), 3, 50);
    });
    
    
    $('#txtName').focusout(function(){
        if(validateLength($('#lblNameError'), $('#txtName'), 3, 50)){
            validateString($('#lblNameError'), $('#txtName'));
        }
    });
    
    
    $('#txtEmail').focusout(function(){
        if(validateLength($('#lblEmailError'), $('#txtEmail'), 3, 30)){
            validateEmail($('#lblEmailError'), $('#txtEmail'));
        }
    });

});