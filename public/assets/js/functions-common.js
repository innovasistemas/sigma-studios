/* 
 * Common functions from website
 */


// Function to load data when loading page
function loadData()
{
    $.ajax({
        url: 'https://sigma-studios.s3-us-west-2.amazonaws.com/test/colombia.json',
//        url: 'http://localhost/sigma-studios/public/assets/api/colombia.json',
        type: 'GET',
        dataType: 'json',
        success: function(data) {
            var option = "<option></option>";
            $.each(data, function(index, value){
                option += "<option value='" + index + "'>" + index + "</option>";
            });
            $('#cboDepartment').html(option);
        }
    }).done(function(){

    });
}


// Function to general validation
function validateData()
{
    var sw = false;
    if(validateLength($('#lblDepartmentError'), $('#cboDepartment'), 3, 30)){
        if(validateLength($('#lblCityError'), $('#cboCity'), 3, 50)){
            if(validateLength($('#lblNameError'), $('#txtName'), 3, 50)){
                if(validateString($('#lblNameError'), $('#txtName'))){
                    if(validateLength($('#lblEmailError'), $('#txtEmail'), 6, 30)){
                        if(validateEmail($('#lblEmailError'), $('#txtEmail'))){
                            sw = true;
                        }
                    }
                }
            }
        }
    }
    return sw;
}


// Function to validate string length between two values
function validateLength(element, input, minLength, maxLength)
{
    var sw = false;
    if(input.val().length >= minLength && input.val().length <= maxLength){
        element.addClass("d-none");
        input.removeClass("text-danger");
        sw = true;
    }else{
        element.text("El campo debe tener una longitud entre " + minLength + " y " + maxLength);
        element.removeClass("d-none");
        input.addClass("text-danger");
    }
    return sw;
}


// Function to validate email
function validateEmail(element, input)
{
    var sw = false;
    var pattern = /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
    if (pattern.test(input.val())){
        element.addClass("d-none");
        input.removeClass("text-danger");
        sw = true;
    } else{
        element.removeClass("d-none");
        input.addClass("text-danger");
    }
    return sw;
}


// Function to validate only characters and spaces
function validateString(element, input)
{
    var sw = false;
    var regExpString=/^[a-zA-ZÑñÁáÉéÍíÓóÚúÜü\s]+$/;
    if(regExpString.test(input.val())){
        element.addClass("d-none");
        input.removeClass("text-danger");
        sw = true;
    }else{
        element.text("El campo solo admite letras y espacios");
        element.removeClass("d-none");
        input.addClass("text-danger");
    }
    return sw;
}


// Function to show information message 
function showMessage(message = "", headerTitle = "Información del sistema")      
{   
    $( "#divInformation" ).html("<strong>" + message + "</strong>");
    $( "#divInformation" ).dialog({
        title: headerTitle,
        autoOpen: true,
        modal: true,
        dialogClass: 'hide-close',
        closeOnEscape: false,
        buttons: {
            "Aceptar": function(){
                $(this).dialog("close"); 
            }
        } 
    });
}


// Function to reset/clean form
function resetForm()
{
    $('#frmRegister').find('input[type=text], input[type=email]').val('');
    $("#frmRegister").find('.select2').val('').trigger('change');
}
